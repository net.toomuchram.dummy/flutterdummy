import 'package:flutter/material.dart';
import 'package:xml/xml.dart' as xml;
import 'package:quiver/collection.dart';
import 'package:xml/xml.dart';
import '../variables.dart';

class MenuList extends DelegatingList<Widget> {
  final List<Widget> _l = [];

  List<Widget> get delegate => _l;

  List<Widget> addInexpandableMenuSection(
      XmlElement section,
      BuildContext context,
      void Function(String type, String key, String title) loaderfunction) {

    if (section.getAttribute("adminOnly") == "true" && !credentialsManager.getAdmin()) {
      return this;
    }
    final sectionName = section.getAttribute("name");
    if (sectionName != "" && sectionName != null) {
      this.add(Divider());
      this.add(
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 5),
          child: Align(
            alignment: Alignment.centerLeft,
            child: Text(
              sectionName,
              style: TextStyle(color: Theme.of(context).dividerColor),
            ),
          ),
        ),
      );
    }
    section.findAllElements("menuitem").forEach((menuelement) {
      final title = menuelement.getAttribute("title");
      final type = menuelement.getAttribute("type");
      final key = menuelement.getAttribute("key");
      final icon = menuelement.getAttribute("icon");
      final adminOnly = menuelement.getAttribute("adminOnly");

      if (!(adminOnly != null &&
          (adminOnly == 'true' && !credentialsManager.getAdmin()))) {
        this.add(
          ListTile(
            leading: Icon(
              iconmapper[icon],
              color: Theme.of(context).iconTheme.color,
            ),
            title: Text(title),
            onTap: () {
              Navigator.pop(context);
              loaderfunction(type, key, title);
            },
          ),
        );
      }
    });
    return this;
  }

  List<Widget> addExpandableMenuSection(
      XmlElement section,
      BuildContext context,
      void Function(String type, String key, String title) loaderfunction) {
    final sectionName = section.getAttribute("name");
    if (section.getAttribute("adminOnly") == "true" && !credentialsManager.getAdmin()) {
      return this;
    }

    if (sectionName == null || sectionName == "") {
      throw new Exception("Section name should not be null");
    }


    this.add(Divider());
    List<Widget> expandableListChildren = List<Widget>();
    section.findAllElements("menuitem").forEach((menuelement) {
      final title = menuelement.getAttribute("title");
      final type = menuelement.getAttribute("type");
      final key = menuelement.getAttribute("key");
      final icon = menuelement.getAttribute("icon");
      final adminOnly = menuelement.getAttribute("adminOnly");

      if (!(adminOnly != null &&
          (adminOnly == 'true' && !credentialsManager.getAdmin()))) {
        expandableListChildren.add(ListTile(
          leading: Icon(
            iconmapper[icon],
            color: Theme.of(context).iconTheme.color,
          ),
          title: Text(title),
          onTap: () {
            Navigator.pop(context);
            loaderfunction(type, key, title);
          },
        ));
      }
    });

    this.add(
      Theme(
        data: Theme.of(context).copyWith(dividerColor: Colors.transparent),
        child: ExpansionTile(
          key: PageStorageKey<String>(sectionName),
          title: new Text(sectionName),
          children: expandableListChildren,
        ),
      ),
    );
    return this;
  }
}

class AndroidDrawer extends StatefulWidget {
  final Future<xml.XmlDocument> xmlfuture;
  final void Function(String type, String key, String title) loaderfunction;

  const AndroidDrawer({@required this.xmlfuture, @required this.loaderfunction});

  @override
  State<StatefulWidget> createState() {
    return _AndroidDrawerState(xmlfuture, loaderfunction);
  }
}

class _AndroidDrawerState extends State<AndroidDrawer> {
  final Future<xml.XmlDocument> _xmlfuture;
  final void Function(String type, String key, String title) loaderfunction;

  _AndroidDrawerState(this._xmlfuture, this.loaderfunction);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 275,
      child: Drawer(
        child: FutureBuilder(
          future: _xmlfuture,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              MenuList children = MenuList();
              children.add(SizedBox(
                height: 125,
                child: DrawerHeader(
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage('assets/images/ic_header.webp'),
                          fit: BoxFit.cover)),
                  child: null,
                ),
              ));

              final xml.XmlDocument document = snapshot.data;
              final sections = document.findAllElements('section');
              sections.forEach((section) {
                final expandable = section.getAttribute("expandable");

                if (expandable == "false" || expandable == null) {
                  children.addInexpandableMenuSection(
                      section, context, loaderfunction);
                } else {
                  children.addExpandableMenuSection(
                      section, context, loaderfunction);
                }
              });

              return ListView(padding: EdgeInsets.zero, children: children);
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          },
        ),
      ),
    );
  }
}
