import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_html/style.dart';
import 'package:url_launcher/url_launcher.dart';

class TextPage extends StatelessWidget {
  final Future<String> filefuture;

  const TextPage({this.filefuture});

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: filefuture,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          final String html = snapshot.data;
          return Container(
            width: double.infinity,
            height: double.infinity,
            alignment: Alignment.topCenter,
            child: SingleChildScrollView(
              child: Html(
                data: html,
                style: {
                  'p': Style(
                    margin: EdgeInsets.all(0.0),
                    padding: EdgeInsets.all(0.0),
                  ),
                  'body': Style(
                      margin: EdgeInsets.only(
                          left: 8.0,
                          right: 8.0,
                          top: 8.0,
                          bottom: 8.0,
                      ),
                  ),
                },
                customRender: {
                  "img": (context, child, attributes, element) {
                    return Image(
                      image: AssetImage(attributes["src"]),
                    );
                  },
                  "center": (context, child, attributes, element) {
                    return Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[child],
                    );
                  },
                },
                onLinkTap: (String url) async{
                  if (await canLaunch(url)) {
                    await launch(url);
                  } else {
                    throw 'Could not launch $url';
                  }
                },
              ),
            ),
          );
        } else {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }
}
