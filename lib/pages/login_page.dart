import 'package:flutter/material.dart';
import 'package:flutterdummy/services/user_service.dart';
import 'package:flutterdummy/variables.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:after_layout/after_layout.dart';

class LoginScreen extends StatefulWidget {
  @override
  LoginScreenState createState() => LoginScreenState();
}

class LoginScreenState extends State<LoginScreen>
    with AfterLayoutMixin<LoginScreen> {
  final _formKey = GlobalKey<FormState>();

  String _username;
  String _password;
  bool _loggedIn = false;

  Widget _submitbutton = Text("loading");

  @override
  Widget build(BuildContext context) {
    loginScreenOpened = true;
    return Material(
      child: WillPopScope(
        child: Container(
          height: double.infinity,
          padding: EdgeInsets.symmetric(horizontal: 50.0),
          child: Center(
            child: Form(
              key: _formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  TextFormField(
                    decoration: const InputDecoration(
                      prefixIcon: Icon(Icons.person),
                      labelText: "Gebruikersnaam",
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Voer een gebruikersnaam in';
                      } else {
                        return null;
                      }
                    },
                    onSaved: (value) {
                      _username = value;
                    },
                  ),
                  TextFormField(
                    decoration: const InputDecoration(
                      prefixIcon: Icon(Icons.vpn_key),
                      labelText: "Wachtwoord",
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Voer een wachtwoord in';
                      } else if (value.length < 7) {
                        return 'Te kort wachtwoord';
                      } else {
                        return null;
                      }
                    },
                    onSaved: (value) {
                      _password = value;
                    },
                    obscureText: true,
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  _submitbutton,
                  SizedBox(
                    height: 30,
                  ),
                  Text(
                    "Door in te loggen ga je akkoord met de privacy policy, te vinden op https://romereis-hl.nl/privacy.html"
                  ),
                ],
              ),
            ),
          ),
        ),
        onWillPop: () {
          // We don't want the user to just nope out of the login screen,
          // so this code blocks the back button on Android.
          return new Future(() => _loggedIn);
        },
      ),
    );
  }

  void _formSubmit() async {
    // Validate returns true if the form is valid, otherwise false.
    if (_formKey.currentState.validate()) {
      final oldsubmitbutton = _submitbutton;

      setState(() {
        _submitbutton = CircularProgressIndicator();
      });

      _formKey.currentState.save();
      final error = await UserService(context).login(_username, _password);

      if (error != null) {
        Fluttertoast.showToast(
          msg: error,
        );
        setState(() {
          _submitbutton = oldsubmitbutton;
        });
      } else {
        Fluttertoast.showToast(
          msg: "Welkom, $_username",
        );
        _loggedIn = true;
        Navigator.of(context).pop();
        loginScreenOpened = false;
        return null;

      }
    }
  }

  @override
  void afterFirstLayout(BuildContext context) {
    setState(() {
      _submitbutton = RaisedButton(
        onPressed: _formSubmit,
        child: Text('Log in'),
      );
    });
  }
}
