import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_html/style.dart';
import 'package:url_launcher/url_launcher.dart';

class DynamicContentPage extends StatelessWidget {
  final Future<String> contentfuture;
  const DynamicContentPage({this.contentfuture});


  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: contentfuture,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          return SingleChildScrollView(
            child: Html(
              data: snapshot.data,
              style: {
                'p': Style(
                  margin: EdgeInsets.all(0.0),
                  padding: EdgeInsets.all(0.0),
                ),
                'body': Style(
                  margin: EdgeInsets.only(
                    left: 8.0,
                    right: 8.0,
                    top: 0.0,
                    bottom: 8.0,
                  ),
                ),
              },
              onLinkTap: (String url) async{
                if (await canLaunch(url)) {
                  await launch(url);
                } else {
                  throw 'Could not launch $url';
                }
              },
            ),
          );
        } else {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }

}