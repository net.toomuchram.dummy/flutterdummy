import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';

class Photopage extends StatelessWidget {
  final AssetImage image;
  final bool includeBackButton;

  const Photopage({@required this.image, this.includeBackButton = false});

  @override
  Widget build(BuildContext context) {
    final imageContainer = Container(
        child: PhotoView(
      imageProvider: image,
      minScale: PhotoViewComputedScale.contained,
      maxScale: PhotoViewComputedScale.covered * 3,
      backgroundDecoration:
          BoxDecoration(color: Theme.of(context).scaffoldBackgroundColor),
    ));

    // If includeBackButton is true, the image container will be put in a
    // Scaffold which includes a backbutton, which, when pressed, causes the
    // current screen to pop
    return (includeBackButton
        ? Scaffold(
            appBar: AppBar(
              leading: IconButton(
                icon: Icon(Icons.arrow_back),
                onPressed: () => {Navigator.pop(context)},
              ),
            ),
            body: imageContainer,
          )
        : imageContainer);
  }
}
