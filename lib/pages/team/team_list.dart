import 'dart:convert';
import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:flutterdummy/pages/team/team_page.dart';
import 'package:flutterdummy/services/romereis_http.dart';
import 'package:flutterdummy/variables.dart';

class TeamList extends StatefulWidget {
  @override
  TeamListState createState() {
    return TeamListState();
  }
}

class TeamListState extends State<TeamList> {

  RomereisHttp _romereisHttp;

  @override
  Widget build(BuildContext context) {
    _romereisHttp = RomereisHttp(context);
    return FutureBuilder(
      future: _romereisHttp.get("$augustusURL/getTeamCategories?sessionId=${credentialsManager.getSessionId()}"),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          if (snapshot.data != null) {
            List<dynamic> teamCategories = List.from(jsonDecode(snapshot.data.body));
            return ListView.builder(
              itemCount: teamCategories.length,
              itemBuilder: (context, i) {
                return ExpandablePanel(
                  header: ListTile(
                    title: Text(teamCategories[i]["name"] == "default" ? "Standaard" : teamCategories[i]["name"]),
                  ),
                  expanded: Padding(
                      padding: EdgeInsets.only(left: 8.0),
                      child: FutureBuilder(
                        future: _romereisHttp.get("$augustusURL/getTeams?sessionId=${credentialsManager.getSessionId()}&categoryId=${teamCategories[i]["id"]}"),
                        builder: (context, snapshot) {
                          if (snapshot.connectionState == ConnectionState.done) {
                            if (snapshot.data != null) {
                              List<dynamic> teamsInCategory = List.from(jsonDecode(snapshot.data.body));

                              return ListView.builder(
                                itemCount: teamsInCategory.length,
                                shrinkWrap: true,
                                itemBuilder: (context, i2) {
                                  return ListTile(
                                    title: Text(teamsInCategory[i2]["name"]),
                                    subtitle: FutureBuilder(
                                      future: _romereisHttp.get("$augustusURL/getTeamMembers?sessionId=${credentialsManager.getSessionId()}&teamId=${teamsInCategory[i2]["id"]}"),
                                      builder: (context, snapshot) {
                                        if (snapshot.connectionState == ConnectionState.done) {
                                          if (snapshot.data != null) {
                                            List<dynamic> usernamelist = List.from(jsonDecode(snapshot.data.body));
                                            if (usernamelist.length < 1) {
                                              return Text(
                                                "Deze groep heeft geen leden",
                                                style: TextStyle(
                                                    fontStyle: FontStyle.italic
                                                ),
                                              );
                                            }
                                            List<TextSpan> usernameWidgetList = [];
                                            for (int i=0; i<usernamelist.length; i++) {
                                              // Make the user's name bold if they are in the team
                                              if (usernamelist[i]["id"] == credentialsManager.getUserId()) {
                                                usernameWidgetList.add(
                                                    TextSpan(text: usernamelist[i]["name"], style: TextStyle(fontWeight: FontWeight.bold))
                                                );
                                              } else {
                                                usernameWidgetList.add(
                                                    TextSpan(text: usernamelist[i]["name"])
                                                );
                                              }

                                              if (i+1 < usernamelist.length) {
                                                usernameWidgetList.add(TextSpan(text: ", "));
                                              }

                                            }
                                            return Text.rich(
                                              TextSpan(
                                                children: usernameWidgetList
                                              ),
                                            );

                                          } else {
                                            return Text("Er is iets fout gegaan");
                                          }
                                        } else {
                                          return Text("Loading");
                                        }
                                      },
                                    ),
                                    onTap: () {
                                      Navigator.push<void>(
                                          context, MaterialPageRoute(builder: (context) => TeamPage(
                                        teamId: teamsInCategory[i2]["id"],
                                        teamName: teamsInCategory[i2]["name"],
                                        teamCategory: teamCategories[i]["name"] == "default" ? "Standaard" : teamCategories[i]["name"],
                                      )));
                                    },
                                  );
                                },
                              );
                            } else {
                              return Center(
                                child: Text('Herlaad deze pagina a.u.b.'),
                              );
                            }
                          } else {
                            return Center(
                              child: Container(
                                margin: EdgeInsets.symmetric(vertical: 8.0),
                                child: CircularProgressIndicator(),
                              ),
                            );
                          }
                        },
                      )),
                  theme: ExpandableThemeData(
                    iconColor: Theme.of(context).accentColor,
                  ),
                );
              },
            );
          } else {
            return Center(
              child: Text('Herlaad deze pagina a.u.b.'),
            );
          }
        } else {
          return Center(child: CircularProgressIndicator());
        }
      },
    );
  }
}
