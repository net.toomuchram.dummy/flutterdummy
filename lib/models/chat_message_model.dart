import 'package:flutter/widgets.dart';


class ChatMessage {
  final int id;
  final String user;
  final String date;
  final String imageURL;
  final String content;

  ChatMessage(
      {@required this.id,
        @required this.user,
        @required this.date,
        @required this.imageURL,
        @required this.content});

  ChatMessage.fromJSON(Map<String, dynamic> json)
      : id = json["id"],
        user = json["user"],
        date = json["timestamp"],
        imageURL = json["imgpath"],
        content = json["message"];
}