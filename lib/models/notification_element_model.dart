class NotificationJsonElement {
  String notiftime;
  String message;
  String username;

  NotificationJsonElement({this.notiftime, this.message, this.username});
}