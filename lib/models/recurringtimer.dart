import 'dart:async';
// https://stackoverflow.com/questions/14946012/how-do-i-run-a-reoccurring-function-in-dart
class RecurringTimer {
  Timer interval(Duration duration, func) {
    Timer function() {
      Timer timer = new Timer(duration, function);

      func(timer);

      return timer;
    }

    return new Timer(duration, function);
  }

}