import 'dart:convert';

import 'package:flutter/src/widgets/framework.dart';
import 'package:flutterdummy/services/romereis_http.dart';
import 'package:http/http.dart' as http;

import '../variables.dart';

class UserService extends RomereisHttp {
  // pass context to parent
  UserService(BuildContext context) : super(context);

  /// Function to test the credentials
  /// Returns a string containing an error, or if there were no errors, returns null.
  Future<String> login(username, password) async {
    http.Response response = await post("$augustusURL/login",
        body: {'username': username, 'password': password});

    if (response.statusCode == 200) {
      final parsedResponse = LoginResponse.fromJson(json.decode(response.body));

      credentialsManager.setUsername(parsedResponse.username);
      credentialsManager.setSessionId(parsedResponse.sessionId);
      credentialsManager.setUserId(parsedResponse.userid);
      credentialsManager.setAdmin(parsedResponse.admin);

      return null;
    }

    if (response == null) {
      return "Onbekende fout";
    } else {
      return errormapper[response.body];
    }
  }

  Future<bool> checkIfAdmin() async {
    final sessionId = credentialsManager.getSessionId();
    final response = await get("$augustusURL/isAdmin?sessionId=$sessionId");
    if (response != null) {
      return response.body == "true";
    }
    return false;
  }

  Future<bool> validateSession() async {

    final sessionId = credentialsManager.getSessionId();

    if (sessionId == null || sessionId == "") {
      return false;
    }

    try {
      final response = await get("$augustusURL/verifySession?sessionId=$sessionId");
      return response.body == "true";
    } catch (e) {
      return true;
    }
  }
}

class LoginResponse {
  String username;
  String sessionId;
  int userid;
  bool admin;

  LoginResponse({this.username, this.sessionId, this.userid, this.admin});

  factory LoginResponse.fromJson(Map<String, dynamic> json) {
    return LoginResponse(
      username: json["username"],
      sessionId: json["sessionId"],
      userid: int.parse(json["userId"].toString()),
      admin: json["admin"] == 1,
    );
  }
}
