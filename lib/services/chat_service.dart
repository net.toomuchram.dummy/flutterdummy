import 'dart:async';
import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutterdummy/models/chat_message_model.dart';
import 'package:flutterdummy/services/romereis_http.dart';
import 'package:flutterdummy/services/websocket_service.dart';
import 'package:flutterdummy/variables.dart';
import 'package:http/http.dart';
import 'package:web_socket_channel/io.dart';

class ChatService extends WebsocketService {

  String teamId;
  final BuildContext context;
  RomereisHttp rhttp;

  /*
    WebSocket-related code
   */
  ChatService({@required this.context, @required this.teamId}) : super(context) {
    this.channel = IOWebSocketChannel.connect(juliusURL);
    this.rhttp = RomereisHttp(context);
    this.initialise();
  }

  @override
  Stream<ChatMessage> createStream() {
    StreamController<ChatMessage> controller;
    StreamSubscription source;

    void processWSMessages() {
      source = channel.stream.listen( (chunk) {
        if (chunk is String) {
          if (kDebugMode) {
            print("Received WebSocket message: $chunk");
          }

          Map<String, dynamic> jsonMessage = jsonDecode(chunk);
          String channel = jsonMessage["channel"];
          switch (channel) {
            case "msg": {
              ChatMessage chatMessage = ChatMessage.fromJSON(jsonMessage);
              print(teamId);
              if (jsonMessage["teamId"].toString() == teamId) {
                controller.add(chatMessage);
              }
              break;
            }
            case "errors": {
              String error = jsonMessage["error"];
              if (invalidSessionErrors.contains(error)) {
                openLoginScreen();
              } else if(kReleaseMode) {
                sentry.captureException(
                  exception: error,
                  stackTrace: "in _createStream(), ChatService",
                );
              }
              break;
            }
            default: {
              break;
            }
          }
        }
      } );
    }

    void cancelProcessing() {
      source.cancel();
      controller.close();
    }

    controller = StreamController<ChatMessage>.broadcast(
        onListen: processWSMessages,
        onCancel: cancelProcessing
    );

    return controller.stream;
  }

  /*
    Message history related code
   */

  // TODO: Message History should be divided by pages instead of dates
  Future<List<ChatMessage>> getMessageHistory(int from, int to) async {
    Response response = await rhttp.get(
        "$augustusURL/getMessages?days=$from&to=$to&teamId=$teamId&sessionId=${credentialsManager.getSessionId()}");
    if (response != null) {
      var jsonArray = List.from(jsonDecode(response.body));
      return List<ChatMessage>.generate(jsonArray.length, (index) {
        return ChatMessage.fromJSON(jsonArray[index]);
      });
    } else {
      return null;
    }
  }

  void sendMessage(String message) {
    rhttp.post("$augustusURL/sendMessage", body: {
      'sessionId': credentialsManager.getSessionId(),
      'teamId': teamId,
      'message': message
    });
  }
}
