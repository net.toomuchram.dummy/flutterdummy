import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutterdummy/models/notification_element_model.dart';
import 'package:flutterdummy/services/romereis_http.dart';
import 'package:http/http.dart';

import '../variables.dart';

class NotificationsHistoryService extends RomereisHttp {

  Future<Response> notificationsFuture;

  final BuildContext context;

  Future<Response> getNotifications() {
    notificationsFuture = get("$augustusURL/getAnnouncements?sessionId=${credentialsManager.getSessionId()}");
    notificationsFuture.then((response) => _handleResponse(response));
    return notificationsFuture;
  }

  void _handleResponse(Response response) {
    List parsedJson = json.decode(response.body);
    notifications = convertJsonToDartObject(List<Map<String, dynamic>>.from(parsedJson));
  }

  List<NotificationJsonElement> notifications;

  NotificationsHistoryService(this.context, {this.notifications}) : super(context);
  
  List<NotificationJsonElement> convertJsonToDartObject(List<Map<String, dynamic>> parsedJson) {
    List<NotificationJsonElement> notifs = List();
    parsedJson.forEach((jsonelement) {
      notifs.add(NotificationJsonElement(
        notiftime: jsonelement["notiftime"],
        message: jsonelement["message"],
        username: jsonelement["username"],
      ));
    });
    return notifs;
  }

  Widget buildNotificationList() {
    if (notifications != null) {
      return ListView.builder(
        itemCount: notifications.length,
        itemBuilder: (BuildContext context, int index) {
          final reversedIndex = notifications.length -1 - index;
          return ListTile(
            title: Text(notifications[reversedIndex].message),
            subtitle: Text("${notifications[reversedIndex].notiftime} · ${notifications[reversedIndex].username}"),
          );
        },
      );
    } else {
      return Text("Fout bij het ophalen van de notificaties");
    }

  }
}