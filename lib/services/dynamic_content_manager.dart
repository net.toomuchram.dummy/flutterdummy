import 'dart:io';
import 'package:flutterdummy/variables.dart';
import 'package:path_provider/path_provider.dart';
import 'package:http/http.dart' as http;

class DynamicContentManager {

  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();

    return directory.path;
  }

  Future<File> _getLocalFile(String filename) async {
    final path = await _localPath;
    return File('$path/$filename');
  }

  Future<String> _writeToFile(String filename, String contents) async {
    final file = await _getLocalFile(filename);
    file.writeAsString(contents);
    return contents;
  }

  Future<String> fetchDynamicContent(String name) async {
    final sessionId = credentialsManager.getSessionId();
    final url = "$augustusURL/dynamicContent?name=$name&sessionId=$sessionId";
    try {
      final response = await http.get(url);
      if (invalidSessionErrors.contains(response.body)) {
        throw NotLoggedInException();
      }
      return response.body;
    }
    catch(e) {
      //We can assume there's no internet, so just return the cached version
      return await _getDynamicContent(name);
    }
  }

  Future<String> _getDynamicContent(String name) async {
    final file = await _getLocalFile(name);
    String contents = await file.readAsString();
    if (contents == null) {
      return '$name has not been downloaded yet.';
    } else {
      return contents;
    }
  }

  Future<String> getGeneralInfo() => _getDynamicContent('info');
  Future<String> getProgramme() => _getDynamicContent('programme');
  Future<String> getTeams() => _getDynamicContent('teams');

  Future<String> fetchGeneralInfo() async => _writeToFile('info', await fetchDynamicContent('info'));
  Future<String> fetchProgramme() async => _writeToFile('programme', await fetchDynamicContent('programme'));
  Future<String> fetchTeams() async => _writeToFile('teams', await fetchDynamicContent('teams'));

}
