import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import '../pages/login_page.dart';
import '../variables.dart';

// A helper class to automatically check the request for an error code
// If an error occurs, show login screen
// Otherwise, show a toast containing the error
class RomereisHttp {

  BuildContext context;
  RomereisHttp(this.context);

  Future<http.Response> get(url, {Map<String, String> headers}) => _checkRequest(http.get(url, headers: headers));
  Future<http.Response> post(url, {Map<String, String> headers, body, Encoding encoding}) => _checkRequest(http.post(url, headers: headers, body: body, encoding: encoding));

  Future<http.Response> _checkRequest(Future<http.Response> request) async {
    http.Response response = await request;

    // fatal error
    if (response == null) {
      Fluttertoast.showToast(
        msg: "Er is een fout opgetreden.",
      );

      // Send the error to Sentry (only in production)
      if (kReleaseMode) {
        try {
          sentry.captureException(
              exception: "Uncaught error in RomereisHttp request: ${response.body}",
              stackTrace: "URL was ${response.request.url} with headers ${response.request.headers.toString()}"
          );
          print('Error sent to sentry.io: ${response.body}');
        } catch (e) {
          print('Sending report to sentry.io failed: $e');
          print('Original error: ${response.body}');
        }
      }

      return null;
    }

    // user should login
    if (invalidSessionErrors.contains(response.body) && !loginScreenOpened) {
      await Navigator.push<void>(context, MaterialPageRoute(builder: (context) => LoginScreen()));
      return response;
    }

    // everything is fine
    if (response.statusCode == 200) {
      return response;
    }

    // return response anyway
    return response;
  }
}