import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutterdummy/models/credentials_manager.dart';
import 'package:sentry/sentry.dart';

final iconmapper = <String, IconData>{
  'info': Icons.info,
  'aantekening': Icons.bookmark,
  'huiswerk': Icons.edit,
  'stamboom': Icons.people,
  "programma": Icons.format_list_bulleted,
  "teams": Icons.people,
  "notificaties": Icons.notifications,
  "verstuuraankondiging": Icons.announcement,
  "chat": Icons.chat,
  "globalchat": Icons.chat_bubble,
  "geotracker": Icons.location_on
};

Map<String, String> errormapper = {
  "ERR_NO_SUCH_USER": "Gebruiker niet gevonden",
  "ERR_WRONG_PASSWORD": "Fout wachtwoord",
};

final augustusURL = DotEnv().env['AUGUSTUS_URL'];
final juliusURL = DotEnv().env["JULIUS_URL"];

final credentialsManager = CredentialsManager();

class NotLoggedInException implements Exception {}

bool loginScreenOpened = false;

final invalidSessionErrors = ["ERR_NO_SUCH_SESSION", "ERR_NO_SUCH_USER", "ERR_MISSING_SESSIONID"];

final SentryClient sentry = new SentryClient(dsn: DotEnv().env["SENTRY_URL"]);

